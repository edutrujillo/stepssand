@isTest

public class TestVerifyDate {
    @isTest static void date2WithinDate1(){
    	Date d1=Date.today();
    	Date d2=d1.addDays(24);
    	System.debug(d1);
    	System.debug(d2);
    	System.debug(VerifyDate.CheckDates(d1,d2));
    	System.assertEquals(d2,VerifyDate.CheckDates(d1,d2));
    }
    
    @isTest static void date2NotWithinDate1(){
    	Date d1=Date.today();
    	Date d2=d1.addDays(-24);
    	Integer totalDays = Date.daysInMonth(d1.year(), d1.month());
    	Date lastDay = Date.newInstance(d1.year(), d1.month(), totalDays);
    	System.assertEquals(lastDay,VerifyDate.CheckDates(d1,d2));
    }
}