<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>actualizaci_n_nombre</fullName>
        <field>Name</field>
        <formula>Nombre__c + &apos;Demostración&apos;</formula>
        <name>actualización nombre</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
</Workflow>
